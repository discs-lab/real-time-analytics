#pragma once
#include <iostream>
#include <vector>
#include <unordered_map>

#include "MapBase.h"

class MapOne : public MapBase {
	public:
		virtual void PrintContents() override;

		int Put(int /*imageID*/, std::vector<int> /*annotation list*/);
		
		std::vector<int> Get(int);

	private:
		std::unordered_map<int, std::vector<int>> store;
};

