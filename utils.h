#pragma once

#include <iostream>
#include <vector>

template<typename T>
void PrintVector(T const &input);

void PrintIntVector(std::vector<int>);

void PrintStringVector(std::vector<std::string>);
