#include <chrono>
#include <inttypes.h>

#include "benchmark.h"
#include "histogram.h"
#include "ImageAnnotationMap.h"
#include "ImageUserTimeSeriesMap.h"
#include "UserAnnotationTimeSeriesMap.h"
#include "UserAnnotationScoresMap.h"
#include "zipf.h"

int DURATION = 7200; // total duration of benchmark in seconds 
int INTERVAL = 2; // interval in which statistics are recorded

int FLAGS_num = 1000000; // number of KV pairs to place in database
int FLAGS_users = 1000;
int FLAGS_pictures = 1000000;
int FLAGS_annotations = 1000000;
int FLAGS_pic_annotations = 5;
double FLAGS_popular_users = 0.01;

void RunBenchmark() {
	//std::cout << "Running benchmark\n";	

	// initialize zipf generator
	init_zipf_generator(0, FLAGS_num);

	// initialize maps
	MapOne MAP_ONE;
	MapTwo MAP_TWO;
	MapThree MAP_THREE;
	MapFour MAP_FOUR;

	// initialize histograms
	Histogram MAP_ONE_HISTOGRAM;
	Histogram MAP_TWO_HISTOGRAM;
	Histogram MAP_THREE_HISTOGRAM;
	Histogram MAP_FOUR_HISTOGRAM;

	// initialize action variables
	int64_t users_done = 0;
	int64_t views = 0;
	int64_t likes = 0;
	int64_t shares = 0;
	int64_t adds = 0;

	// initialize operation variables
	int64_t found = 0;
	int64_t map2_reads = 0;
	int64_t map3_reads = 0;
	int64_t map4_reads = 0;
	int64_t map1_writes = 0;
	int64_t map2_writes = 0;
	int64_t map3_writes = 0;
	int64_t map4_writes = 0;
	int64_t total_reads_done = 0;
	int64_t total_writes_done = 0;
	std::unordered_map<int, std::vector<int>> picture_store;

	clock_t start, end;
	double cpu_time_used;

	// starting timer
	start = clock();

	// loop will run until timer is up
	auto Start = std::chrono::high_resolution_clock::now();
	auto IntervalStart = std::chrono::high_resolution_clock::now();
	while (1) {
		// interval timer logic
		auto IntervalEnd = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double, std::milli> IntervalElapsed = IntervalEnd - IntervalStart;
	
		// general timer logic 
		auto End = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double, std::milli> Elapsed = End - Start;
		
		if (IntervalElapsed.count() >= INTERVAL * 1000.0) {
			printf("Time elapsed: %6f seconds\n", Elapsed.count()/1000);
			printf("Total Writes done: %ld operations\n", total_writes_done);
			printf("Total Reads done: %ld operations\n", total_reads_done);
			printf("------\n");
			printf("Map1 Write: %ld operations\n", map1_writes);
			printf("Map1 Write 50th Percentile: %6f us\n", MAP_ONE_HISTOGRAM.GetPercentile(1, 50)); 
			printf("Map1 Write 99th Percentile: %6f us\n", MAP_ONE_HISTOGRAM.GetPercentile(1, 99));
			printf("---\n");
			printf("Map2 Write: %ld operations\n", map2_writes);
			printf("Map2 Write 50th Percentile: %6f us\n", MAP_TWO_HISTOGRAM.GetPercentile(1, 50));
			printf("Map2 Write 99th Percentile: %6f us\n", MAP_TWO_HISTOGRAM.GetPercentile(1, 99));
			printf("---\n");
			printf("Map3 Write: %ld operations\n", map3_writes);
			printf("Map3 Write 50th Percentile: %6f us\n", MAP_THREE_HISTOGRAM.GetPercentile(1, 50));
			printf("Map3 Write 99th Percentile: %6f us\n", MAP_THREE_HISTOGRAM.GetPercentile(1, 99));
			printf("---\n");
			printf("Map4 Write: %ld operations\n", map4_writes);
			printf("Map4 Write 50th Percentile: %6f us\n", MAP_FOUR_HISTOGRAM.GetPercentile(1, 50));
			printf("Map4 Write 99th Percentile: %6f us\n", MAP_FOUR_HISTOGRAM.GetPercentile(1, 99));
			printf("---\n");
			printf("Map2 Read: %ld operations\n", map2_reads);
			printf("Map2 Read 50th Percentile: %6f us\n", MAP_TWO_HISTOGRAM.GetPercentile(0, 50));
			printf("Map2 Read 99th Percentile: %6f us\n", MAP_TWO_HISTOGRAM.GetPercentile(0, 99));
			printf("---\n");
			
			
			printf("Map3 Read: %ld operations\n", map3_reads);
			printf("Map3 Read 50th Percentile: %6f us\n", MAP_THREE_HISTOGRAM.GetPercentile(0, 50));
			printf("Map3 Read 99th Percentile: %6f us\n", MAP_THREE_HISTOGRAM.GetPercentile(0, 99));
			printf("---\n");
			
			
			printf("Map4 Read: %ld operations\n", map4_reads);
			printf("Map4 Read 50th Percentile: %6f us\n", MAP_FOUR_HISTOGRAM.GetPercentile(0, 50));
			printf("Map4 Read 99th Percentile: %6f us\n", MAP_FOUR_HISTOGRAM.GetPercentile(0, 99));
			printf("---\n");
						
			std::cout << std::endl;
			IntervalStart = std::chrono::high_resolution_clock::now();
		}

		if (Elapsed.count() >= DURATION * 1000.0) break;
		// 1. choose random user
		int CURRENT_USER = std::rand() % FLAGS_users;
		users_done++;

		// 2. choose action for user
		// 	a) View picture (60%)
		// 	b) Like picture (20%)
		//	c) Share picture (15%)
		// 	d) Add picture (5%)
		int ACTION = std::rand() % 100;  		
			
		// 3. choose picture to perform action on
		int CURRENT_PICTURE = std::rand() % FLAGS_pictures;

		// 4. create annotations for picture if DNE
		int count = 0;
		std::vector<int> annotation_list;

		// if picture already exists
		if (picture_store.find(CURRENT_PICTURE) != picture_store.end()) {
			annotation_list = picture_store.at(CURRENT_PICTURE);
		} else { // picture DNE
			// generate annotations using Zipfian			
			while (count < FLAGS_pic_annotations) {
				int k = nextValue() % FLAGS_annotations;
				annotation_list.push_back(k);
				count++;
			}
			picture_store.insert(std::make_pair(CURRENT_PICTURE, annotation_list));
			// INSERTING MAP 1
			auto map1_put_start = std::chrono::high_resolution_clock::now();
			MAP_ONE.Put(CURRENT_USER, annotation_list);
			auto map1_put_elapsed = std::chrono::high_resolution_clock::now() - map1_put_start;
			long map1_put_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map1_put_elapsed).count();
			MAP_ONE_HISTOGRAM.AddWriteStat(map1_put_microseconds);
			map1_writes++;
			total_writes_done++;
		}
		
		int ACTION_SCORE = -1; // score corresponding to current action

		if (ACTION < 60) {
			// view
			views++;
			ACTION_SCORE = 1;
		} else if (ACTION >= 60 && ACTION < 80) {
			// like
			likes++;
			ACTION_SCORE = 2;
		} else if (ACTION >= 80 && ACTION < 95) {
			// share
			shares++;
			ACTION_SCORE = 3;
		} else {
			// add
			adds++;
			ACTION_SCORE = 4;
		}
		
		// get current timestamp as string
		char new_time[100];
		time_t new_now = time(NULL);
		struct tm *t1 = localtime(&new_now);
		strftime(new_time, sizeof(new_time), "%F_%H:%M:%S", t1);
		
		// INSERTING MAP 2
		auto map2_put_start = std::chrono::high_resolution_clock::now();
		MAP_TWO.Put(CURRENT_USER, CURRENT_PICTURE, ACTION_SCORE, new_time);
		auto map2_put_elapsed = std::chrono::high_resolution_clock::now() - map2_put_start;
		long map2_put_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map2_put_elapsed).count();
		MAP_TWO_HISTOGRAM.AddWriteStat(map2_put_microseconds);
		
		map2_writes++;
		total_writes_done++;

		auto map2_get_start = std::chrono::high_resolution_clock::now();	
		int map2_score = MAP_TWO.Get(CURRENT_USER, CURRENT_PICTURE, ACTION_SCORE);
		auto map2_get_elapsed = std::chrono::high_resolution_clock::now() - map2_get_start;
		long map2_get_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map2_get_elapsed).count();
		MAP_TWO_HISTOGRAM.AddReadStat(map2_get_microseconds);

		map2_reads++;
		total_reads_done++;
		// INSERTING MAP 3 & 4
		for (auto const& annotation : annotation_list) {
			// action_score = action in terms of value
			auto map3_put_start = std::chrono::high_resolution_clock::now();
			MAP_THREE.Put(CURRENT_USER, annotation, ACTION_SCORE, new_time);
			auto map3_put_elapsed = std::chrono::high_resolution_clock::now() - map3_put_start;
			long map3_put_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map3_put_elapsed).count();
			MAP_THREE_HISTOGRAM.AddWriteStat(map3_put_microseconds);
			
			map3_writes++;
			total_writes_done++;

			auto map4_put_start = std::chrono::high_resolution_clock::now();	
			MAP_FOUR.Put(CURRENT_USER, annotation, ACTION_SCORE, ACTION_SCORE);
			auto map4_put_elapsed = std::chrono::high_resolution_clock::now() - map4_put_start;
			long map4_put_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map4_put_elapsed).count();
			MAP_FOUR_HISTOGRAM.AddWriteStat(map4_put_microseconds);
			
			map4_writes++;
			total_writes_done++;
		}
		
		auto map3_get_start = std::chrono::high_resolution_clock::now();	
		int map3_score = MAP_THREE.Get(CURRENT_USER, annotation_list.front(), ACTION_SCORE);
		auto map3_get_elapsed = std::chrono::high_resolution_clock::now() - map3_get_start;
		long map3_get_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map3_get_elapsed).count();
		MAP_THREE_HISTOGRAM.AddReadStat(map3_get_microseconds);
		map3_reads++;
		total_reads_done++;

		auto map4_get_start = std::chrono::high_resolution_clock::now();	
		int map4_score = MAP_FOUR.Get(CURRENT_USER, annotation_list.front(), ACTION_SCORE);
		auto map4_get_elapsed = std::chrono::high_resolution_clock::now() - map4_get_start;
		long map4_get_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map4_get_elapsed).count();
		MAP_FOUR_HISTOGRAM.AddReadStat(map4_get_microseconds);
		map4_reads++;
		total_reads_done++;
		
		// create recursive effect for SHARE action
		if (ACTION_SCORE == 4) {
			int num_followers = 0;
			if (CURRENT_USER < FLAGS_users * FLAGS_popular_users) {
				// if user is 'popular'
				num_followers = std::rand() % FLAGS_users;
			} else {
				// if user is not 'popular'
				num_followers = std::rand() % (int)(FLAGS_users*FLAGS_popular_users); 
			}
			int curr_follower = 0;
			while (curr_follower < num_followers) {
				// 1. choose random user
				int NESTED_CURRENT_USER = std::rand() % FLAGS_users;
				users_done++;

				// 2. choose action for user
				// 	a) View picture (60%)
				// 	b) Like picture (20%)
				//	c) Share picture (15%)
				// 	d) Add picture (5%)
				int NESTED_ACTION = std::rand() % 100;  		
					
				// 3. choose picture to perform action on
				int NESTED_CURRENT_PICTURE = std::rand() % FLAGS_pictures;

				// 4. create annotations for picture if DNE
				int nested_count = 0;
				std::vector<int> nested_annotation_list;

				// if picture already exists
				if (picture_store.find(NESTED_CURRENT_PICTURE) != picture_store.end()) {
					nested_annotation_list = picture_store.at(NESTED_CURRENT_PICTURE);
				} else { // picture DNE
					// generate annotations using Zipfian			
					while (nested_count < FLAGS_pic_annotations) {
						int k = nextValue() % FLAGS_annotations;
						nested_annotation_list.push_back(k);
						nested_count++;
					}
					picture_store.insert(std::make_pair(NESTED_CURRENT_PICTURE, annotation_list));
					// INSERTING MAP 1
					auto map1_put_start = std::chrono::high_resolution_clock::now();
					MAP_ONE.Put(NESTED_CURRENT_USER, nested_annotation_list);
					auto map1_put_elapsed = std::chrono::high_resolution_clock::now() - map1_put_start;
					long map1_put_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map1_put_elapsed).count();
					MAP_ONE_HISTOGRAM.AddWriteStat(map1_put_microseconds);
					map1_writes++;
					total_writes_done++;
				}
				
				int NESTED_ACTION_SCORE = -1; // score corresponding to current action

				if (NESTED_ACTION < 60) {
					// view
					views++;
					NESTED_ACTION_SCORE = 1;
				} else if (NESTED_ACTION >= 60 && NESTED_ACTION < 80) {
					// like
					likes++;
					NESTED_ACTION_SCORE = 2;
				} else if (NESTED_ACTION >= 80 && NESTED_ACTION < 95) {
					// share
					shares++;
					NESTED_ACTION_SCORE = 3;
				} else {
					// add
					adds++;
					NESTED_ACTION_SCORE = 4;
				}
				
				// get current timestamp as string
				char nested_new_time[100];
				time_t nested_new_now = time(NULL);
				struct tm *t1 = localtime(&nested_new_now);
				strftime(nested_new_time, sizeof(nested_new_time), "%F_%H:%M:%S", t1);
				// INSERTING MAP 2
				auto map2_put_start = std::chrono::high_resolution_clock::now();	
				MAP_TWO.Put(NESTED_CURRENT_USER, NESTED_CURRENT_PICTURE, NESTED_ACTION_SCORE, new_time);
				auto map2_put_elapsed = std::chrono::high_resolution_clock::now() - map2_put_start;
				long map2_put_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map2_put_elapsed).count();
				MAP_TWO_HISTOGRAM.AddWriteStat(map2_put_microseconds);
				map2_writes++;
				total_writes_done++;
				
				auto map2_get_start = std::chrono::high_resolution_clock::now();	
				int map2_score = MAP_TWO.Get(NESTED_CURRENT_USER, NESTED_CURRENT_PICTURE, NESTED_ACTION_SCORE);
				auto map2_get_elapsed = std::chrono::high_resolution_clock::now() - map2_get_start;
				long map2_get_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map2_get_elapsed).count();
				MAP_TWO_HISTOGRAM.AddReadStat(map2_get_microseconds);

				map2_reads++;
				total_reads_done++;
		

				// INSERTING MAP 3 & 4
				for (auto const& annotation : nested_annotation_list) {
					// action_score = action in terms of value
					auto map3_put_start = std::chrono::high_resolution_clock::now();
					MAP_THREE.Put(NESTED_CURRENT_USER, annotation,NESTED_ACTION_SCORE, new_time);
					auto map3_put_elapsed = std::chrono::high_resolution_clock::now() - map3_put_start;
					long map3_put_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map3_put_elapsed).count();
					MAP_THREE_HISTOGRAM.AddWriteStat(map3_put_microseconds);	
					map3_writes++;
					total_writes_done++;
				
					auto map4_put_start = std::chrono::high_resolution_clock::now();		
					MAP_FOUR.Put(NESTED_CURRENT_USER, annotation, NESTED_ACTION_SCORE, NESTED_ACTION_SCORE);
					auto map4_put_elapsed = std::chrono::high_resolution_clock::now() - map4_put_start;
					long map4_put_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map4_put_elapsed).count();
					MAP_FOUR_HISTOGRAM.AddWriteStat(map4_put_microseconds);
					map4_writes++;
					total_writes_done++;
				}
					
				auto map3_get_start = std::chrono::high_resolution_clock::now();	
				int map3_score = MAP_THREE.Get(NESTED_CURRENT_USER, nested_annotation_list.front(), NESTED_ACTION_SCORE);
				auto map3_get_elapsed = std::chrono::high_resolution_clock::now() - map3_get_start;
				long map3_get_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map3_get_elapsed).count();
				MAP_THREE_HISTOGRAM.AddReadStat(map3_get_microseconds);
				map3_reads++;
				total_reads_done++;

				auto map4_get_start = std::chrono::high_resolution_clock::now();	
				int map4_score = MAP_FOUR.Get(NESTED_CURRENT_USER, nested_annotation_list.front(), NESTED_ACTION_SCORE);
				auto map4_get_elapsed = std::chrono::high_resolution_clock::now() - map4_get_start;
				long map4_get_microseconds = std::chrono::duration_cast<std::chrono::microseconds>(map4_get_elapsed).count();
				MAP_FOUR_HISTOGRAM.AddReadStat(map4_get_microseconds);
				map4_reads++;
				total_reads_done++;

				curr_follower++;
			}
		}
	
	} // while(1)

	end = clock();
	cpu_time_used = ((double)(end-start))/CLOCKS_PER_SEC;

	/*
	printf("\n--------------BENCHMARK RESULTS------------------\n");
        printf("Total CPU time elapsed  : %10f seconds\n", cpu_time_used);
        printf("-------------------------------------------------\n");
        printf("Total users processed   : %10ld \n", users_done);
        printf("Total unique pictures   : %10ld [%f%%]\n", picture_store.size(), (double)picture_store.size()/users_done*100);
        printf("-------------------------------------------------\n");
        printf("Views performed         : %10ld [%f%%]\n", views, (double)views/users_done*100);
        printf("Likes performed         : %10ld [%f%%]\n", likes, (double)likes/users_done*100);
        printf("Shares performed        : %10ld [%f%%]\n", shares, (double)shares/users_done*100);
        printf("Adds performed          : %10ld [%f%%]\n", adds, (double)adds/users_done*100);
	*/
}

