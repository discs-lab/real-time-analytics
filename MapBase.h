#pragma once
#include <iostream>

class MapBase {
	public: 
		virtual void PrintContents() = 0;	

		virtual ~MapBase() {}
};
