#include "ImageAnnotationMap.h"
#include "RWLock.h"

RWLock Map1_lock;

int MapOne::Put(int key, std::vector<int> annotation_list) {
	std::pair<int, std::vector<int>> value(key, annotation_list);
	Map1_lock.WriteLock();
	store.insert(value);
	Map1_lock.WriteUnlock();
	return 0;
}

std::vector<int> MapOne::Get(int imageID) {
	Map1_lock.ReadLock();
	auto imageID_it = store.find(imageID);
	if (imageID_it != store.end()) {
		Map1_lock.ReadUnlock();
		return imageID_it->second;
	}
	Map1_lock.ReadUnlock();	
	printf("ImageID [%d] DNE\n", imageID);
	return {};
}

void MapOne::PrintContents() {
	for (const auto& imageID : store) {
		printf("ImageID: %d\n", imageID.first);
		auto vector = imageID.second;
		std::cout << "\tValue: [ ";
		for (auto i : vector) {
			std::cout << i << ", ";
		}
		std::cout << "]\n";
	}
}
