#include "ImageUserTimeSeriesMap.h"
#include "RWLock.h"
#include "utils.h"

RWLock Map2_lock;

// Multi-step process in adding entry to MapTwo
// 1) Check if userID exists already.
// 	a) If userID already exists, check if imageID exists already.
//		i) If imageID already exists, check if actionID exists already.
//			-> If actionID already exists, append to end of list and increment counter
// 			-> If actionID DNE, add new actionID->Pair(List,Count) entry
//		ii) If imageID DNE, add new actionID->Pair(List,Count) entry in map
//	b) if userID DNE, insert new entry in new maps (with default list/count) and insert to store
int MapTwo::Put(int userID, int imageID, int actionID, std::string timestamp) {
	Map2_lock.WriteLock();
	auto userID_it = store.find(userID);
	// userID key already exists
	if (userID_it != store.end()) {
//		std::cout << "UserID key already exists" << std::endl;
		auto image_map = store[userID];
		auto imageID_it = image_map.find(imageID);

		// imageID key already exists
		if (imageID_it != image_map.end()) {
//			std::cout << "ImageID key already exists" << std::endl;
			auto action_map = image_map[imageID];
			auto action_it = action_map.find(actionID);

			// actionID key already exists
			if (action_it != action_map.end()) {
//				std::cout << "ActionID key already exists" << std::endl;
				action_map[actionID].first.push_back(timestamp);
				action_map[actionID].second += 1;
			} else { // actionID DNE
//				std::cout << "ActionID key DNE" << std::endl;
				std::vector<std::string> time_list;
				time_list.push_back(timestamp);
				
				action_map.insert(std::make_pair(actionID, std::make_pair(time_list, 1)));
			}
			store[userID][imageID] = action_map;

		} else { // imageID key DNE
//			std::cout << "AnnotationID key DNE" << std::endl;
			std::vector<std::string> time_list;
			time_list.push_back(timestamp);

			std::unordered_map<int, std::pair<std::vector<std::string>, int>> innerValueMap;
			innerValueMap.insert(std::make_pair(actionID, std::make_pair(time_list, 1))); 
			image_map.insert(std::make_pair(imageID, innerValueMap));
			store[userID] = image_map;	
		} 	
		
	} else { // userID key DNE
//		std::cout << "UserID key DNE" << std::endl;
		std::vector<std::string> time_list;
		time_list.push_back(timestamp);

		std::unordered_map<int, std::pair<std::vector<std::string>, int>> innerValueMap;
		innerValueMap.insert(std::make_pair(actionID, std::make_pair(time_list, 1))); 

		std::unordered_map<int, std::unordered_map<int, std::pair<std::vector<std::string>, int>>> outerValueMap;
		outerValueMap.insert(std::make_pair(imageID, innerValueMap));
			
		store.insert(std::make_pair(userID, outerValueMap));
	}
	Map2_lock.WriteUnlock();
	return 0;
}

// Returns counter associated with given keys
// If any of the keys DNE, returns -1
int MapTwo::Get(int userID, int imageID, int actionID) {
	Map2_lock.ReadLock();
	auto userID_it = store.find(userID);

	// userID key already exists
	if (userID_it != store.end()) {
		auto image_map = store[userID];
		auto imageID_it = image_map.find(imageID);
	
		// imageID key already exists
		if (imageID_it != image_map.end()) {
			auto action_map = image_map[imageID];
			auto actionID_it = action_map.find(actionID);
		
			// actionID key already exists
			if (actionID_it != action_map.end()) {
				Map2_lock.ReadUnlock();
				return store[userID][imageID][actionID].second;
			}
		}
	}
	Map2_lock.ReadUnlock();
	return -1;
}

void MapTwo::PrintContents() {
	for (auto const& userID : store) {
		printf("UserID: %d\n", userID.first);
		auto image_map = userID.second;
		for (auto const& imageID : image_map) {
			printf("\tImageID: %d\n", imageID.first);
			auto action_map = imageID.second;
			for (auto const& actionID : action_map) {
				printf("\t\tActionID: %d\n", actionID.first);
				printf("\t\t\t Timestamps: ");
				PrintStringVector(actionID.second.first);
				printf("\t\t\t Count: %d\n", actionID.second.second);
			}
		}
	}
}
