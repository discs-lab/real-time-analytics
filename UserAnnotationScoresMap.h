#pragma once
#include <iostream>
#include <unordered_map>

#include "MapBase.h"

class MapFour : public MapBase {
public:
	virtual void PrintContents() override;

	// Enters actionID -> score entry inside of store
	// Uses three layer map:
	//	Layer 1) userID is key
	// 	Layer 2) annotationID is key
	//	Layer 3) actionID is key
	int Put(int /*userID*/, int /*annotationID*/, int /*actionID*/, int /*score*/);

	// Returns score associated to given keys
	// Returns -1 if any of the keys don't exist;
	int Get(int /*userID*/, int /*annotationID*/, int /*actionID*/);

private:
	std::unordered_map<int, std::unordered_map<int, std::unordered_map<int, int>>> store;
};

