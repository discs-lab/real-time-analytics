#!/bin/bash

filename=`date "+%Y-%m-%d_%T"`

echo "current filename: ${filename}"

./test > LOG_FILES/${filename}.txt

echo "Benchmark complete"
