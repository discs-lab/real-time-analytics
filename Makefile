CC=g++
CFLAGS=-g
srcs = $(wildcard *.cc)
objs = $(srcs:.cc=.o)
deps = $(srcs:.cc=.d)

test: ${objs}
	$(CC) $^ -o $@

%.o: %.cc
	$(CC) -MMD -MP -c $< -o $@

.PHONY: clean
clean:
	$(RM) $(objs) $(deps) test

-include $(deps)
