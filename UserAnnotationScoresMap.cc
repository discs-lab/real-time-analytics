#include "UserAnnotationScoresMap.h"
#include "RWLock.h"

RWLock Map4_lock;

// Multi-step process in adding entry to MapFour
// 1) Check if userID exists already.
// 	a) If userID already exists, check if annotationID exists already.
//		i) If annotationID already exists, check if actionID exists already.
//			-> If actionID already exists, increment it by score value
//			-> If actionID DNE, add actionID->score entry in map
//		ii) If annotationID DNE, add actionID->score entry to new map and add new map
//	b) If userID DNE, insert new entry in new maps (two) and insert to store
int MapFour::Put(int userID /*key*/, int annotationID, int actionID, int score) {
	Map4_lock.WriteLock();	
	auto userID_it = store.find(userID);
//	std::cout << "Calling Put() on MapFour" << std::endl;	
	// userID key already exists
	if (userID_it != store.end()) {		
//		std::cout << "UserID key already exists" << std::endl;
		auto annotation_map = store[userID]; 
		auto annotationID_it = annotation_map.find(annotationID);
		
		// annotationID key already exists
		if (annotationID_it != annotation_map.end()) { 
//			std::cout << "AnnotationID key already exists" << std::endl;
			auto action_map = annotation_map[annotationID];
			auto action_it = action_map.find(actionID);

			// actionID key already exists
			if (action_it != action_map.end()) {
//				std::cout << "ActionID key already exists" << std::endl;
				action_map[actionID] += score;
			} else { // actionID DNE
//				std::cout << "ActionID key DNE" << std::endl;
				action_map.insert(std::make_pair(actionID, score));	
			}
			store[userID][annotationID] = action_map;	
		} else { // annotationID key DNE			
//			std::cout << "AnnotationID key DNE" << std::endl;
			std::unordered_map<int, int> innerValueMap;
			innerValueMap.insert(std::make_pair(actionID, score));

			annotation_map.insert(std::make_pair(annotationID, innerValueMap));
			store[userID] = annotation_map;
		}

	} else { // userID key DNE
//		std::cout << "UserID key DNE" << std::endl;
		std::unordered_map<int, int> innerValueMap;
		innerValueMap.insert(std::make_pair(actionID, score));
		
		std::unordered_map<int, std::unordered_map<int, int>> outerValueMap;
		outerValueMap.insert(std::make_pair(annotationID, innerValueMap));

		store.insert(std::make_pair(userID, outerValueMap));
	}
	Map4_lock.WriteUnlock();
	return 0;	
}

// Returns score associated to given userID, annotationID, actionID
// Returns -1 if any of the keys DNE
int MapFour::Get(int userID, int annotationID, int actionID) {
	Map4_lock.ReadLock();
	auto userID_it = store.find(userID);
	// userID key already exists
	if (userID_it != store.end()) {
		auto annotation_map = store[userID];
		auto annotationID_it = annotation_map.find(annotationID);
		// annotationID key already exists
		if (annotationID_it != annotation_map.end()) {
			auto action_map = annotation_map[actionID];
			auto action_it = action_map.find(actionID);
			// actionID key already exists
			if (action_it != action_map.end()) {
				Map4_lock.ReadUnlock();
				return action_map[actionID];
			}
		}
	}
	Map4_lock.ReadUnlock();
	return -1;
}

void MapFour::PrintContents() {
	for (auto const& userID : store) {
		printf("UserID: %d\n", userID.first);
		auto annotation_map = userID.second;
		for (auto const& annotationID : annotation_map) {
			printf("\tAnnotationID: %d\n", annotationID.first);
			auto action_map = annotationID.second;
			for (auto const& actionID : action_map) {
				printf("\t\tActionID: %d -> %d [score]\n", actionID.first, actionID.second);
				
			}
		}
	}
}
