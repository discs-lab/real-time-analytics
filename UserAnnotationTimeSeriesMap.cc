#include "UserAnnotationTimeSeriesMap.h"
#include "RWLock.h"
#include "utils.h"

RWLock Map3_lock;

// Multi-step process in adding entry to MapThree
// 1) Check if userID exists already.
// 	a) If userID already exists, check if annotationID exists already.
//		i) If annotationID already exists, check if actionID exists already.
//			-> If actionID already exists, append to end of list and increment counter
// 			-> If actionID DNE, add new actionID->Pair(List,Count) entry
//		ii) If annotationID DNE, add new actionID->Pair(List,Count) entry in map
//	b) if userID DNE, insert new entry in new maps (with default list/count) and insert to store
int MapThree::Put(int userID, int annotationID, int actionID, std::string timestamp) {
	Map3_lock.WriteLock();
	auto userID_it = store.find(userID);
	// userID key already exists
	if (userID_it != store.end()) {
//		std::cout << "UserID key already exists" << std::endl;
		auto annotation_map = store[userID];
		auto annotationID_it = annotation_map.find(annotationID);

		// annotationID key already exists
		if (annotationID_it != annotation_map.end()) {
//			std::cout << "AnnotationID key already exists" << std::endl;
			auto action_map = annotation_map[annotationID];
			auto action_it = action_map.find(actionID);

			// actionID key already exists
			if (action_it != action_map.end()) {
//				std::cout << "ActionID key already exists" << std::endl;
				action_map[actionID].first.push_back(timestamp);
				action_map[actionID].second += 1;
			} else { // actionID DNE
//				std::cout << "ActionID key DNE" << std::endl;
				std::vector<std::string> time_list;
				time_list.push_back(timestamp);
				
				action_map.insert(std::make_pair(actionID, std::make_pair(time_list, 1)));
			}
			store[userID][annotationID] = action_map;

		} else { // annotationID key DNE
//			std::cout << "AnnotationID key DNE" << std::endl;
			std::vector<std::string> time_list;
			time_list.push_back(timestamp);

			std::unordered_map<int, std::pair<std::vector<std::string>, int>> innerValueMap;
			innerValueMap.insert(std::make_pair(actionID, std::make_pair(time_list, 1))); 
			annotation_map.insert(std::make_pair(annotationID, innerValueMap));
			store[userID] = annotation_map;	
		} 	
		
	} else { // userID key DNE
//		std::cout << "UserID key DNE" << std::endl;
		std::vector<std::string> time_list;
		time_list.push_back(timestamp);

		std::unordered_map<int, std::pair<std::vector<std::string>, int>> innerValueMap;
		innerValueMap.insert(std::make_pair(actionID, std::make_pair(time_list, 1))); 

		std::unordered_map<int, std::unordered_map<int, std::pair<std::vector<std::string>, int>>> outerValueMap;
		outerValueMap.insert(std::make_pair(annotationID, innerValueMap));
			
		store.insert(std::make_pair(userID, outerValueMap));
	}
	Map3_lock.WriteUnlock();
	return 0;
}

// Returns counter associated with given keys
// If any of the keys DNE, returns -1
int MapThree::Get(int userID, int annotationID, int actionID) {
	Map3_lock.ReadLock();
	auto userID_it = store.find(userID);

	// userID key already exists
	if (userID_it != store.end()) {
		auto annotation_map = store[userID];
		auto annotationID_it = annotation_map.find(annotationID);
	
		// annotationID key already exists
		if (annotationID_it != annotation_map.end()) {
			auto action_map = annotation_map[annotationID];
			auto actionID_it = action_map.find(actionID);
		
			// actionID key already exists
			if (actionID_it != action_map.end()) {
				Map3_lock.ReadUnlock();
				return store[userID][annotationID][actionID].second;
			}
		}
	}
	Map3_lock.ReadUnlock();
	return -1;
}


void MapThree::PrintContents() {
	for (auto const& userID : store) {
		printf("UserID: %d\n", userID.first);
		auto image_map = userID.second;
		for (auto const& annotationID : image_map) {
			printf("\tAnnotationID: %d\n", annotationID.first);
			auto action_map = annotationID.second;
			for (auto const& actionID : action_map) {
				printf("\t\tActionID: %d\n", actionID.first);
				printf("\t\t\t Timestamps: ");
				PrintStringVector(actionID.second.first);
				printf("\t\t\t Count: %d\n", actionID.second.second);
			}
		}
	}
}
