#include "histogram.h"

// time in us
const long BUCKET_0_BOTTOM = 0;
const long BUCKET_1_BOTTOM = 1;
const long BUCKET_2_BOTTOM = 5;
const long BUCKET_3_BOTTOM = 10;
const long BUCKET_4_BOTTOM = 50;
const long BUCKET_5_BOTTOM = 100;
const long BUCKET_6_BOTTOM = 1000;
const long BUCKET_7_BOTTOM = 10000;
const long BUCKET_7_TOP = 100000;

// helper function to return associated bucket number to given time
int GetBucketNumber(long time) {
	if (BUCKET_0_BOTTOM <= time && time < BUCKET_1_BOTTOM) {
		return 0;
	} else if (BUCKET_1_BOTTOM <= time && time < BUCKET_2_BOTTOM) {
		return 1;
	} else if (BUCKET_2_BOTTOM <= time && time < BUCKET_3_BOTTOM) {
		return 2;
	} else if (BUCKET_3_BOTTOM <= time && time < BUCKET_4_BOTTOM) {
		return 3;
	} else if (BUCKET_4_BOTTOM <= time && time < BUCKET_5_BOTTOM) {
		return 4;
	} else if (BUCKET_5_BOTTOM <= time && time < BUCKET_6_BOTTOM) {
		return 5;
	} else if (BUCKET_6_BOTTOM <= time && time < BUCKET_7_BOTTOM) {
		return 6;
	} else if (BUCKET_7_BOTTOM <= time && time < BUCKET_7_TOP) {
		return 7;
	}
	return -1;
}

double GetBucketAverage(int bucket) {
	switch(bucket) {
		case 0:
			return (BUCKET_0_BOTTOM + BUCKET_1_BOTTOM) / 2.0;
		case 1:
			return (BUCKET_1_BOTTOM + BUCKET_2_BOTTOM) / 2.0;
		case 2:
			return (BUCKET_2_BOTTOM + BUCKET_3_BOTTOM) / 2.0;
		case 3:
			return (BUCKET_3_BOTTOM + BUCKET_4_BOTTOM) / 2.0;
		case 4:
			return (BUCKET_4_BOTTOM + BUCKET_5_BOTTOM) / 2.0;
		case 5:
			return (BUCKET_5_BOTTOM + BUCKET_6_BOTTOM) / 2.0;
		case 6:
			return (BUCKET_6_BOTTOM + BUCKET_7_BOTTOM) / 2.0;
		case 7:
			return (BUCKET_7_BOTTOM + BUCKET_7_TOP) / 2.0;
		default:
			break;
	}
	return -1;
}

void Histogram::PrintReadCount() {
	for (int i=0; i<7; i++) {
		printf("Bucket %d: %d\n", i, collection[i].first);
	}
}

void Histogram::PrintWriteCount() {
	for (int i=0; i<7; i++) {
		printf("Bucket %d: %d\n", i, collection[i].second);
	}
}

int Histogram::GetBucketFromReadElement(int elementToFind){
	int total = 0;
	for (int i=7; i>=0; i--) {
		total += collection[i].first;
		if (elementToFind < total) return i;
	}
	return 0;
}

int Histogram::GetBucketFromWriteElement(int elementToFind){
	int total = 0;
	for (int i=7; i>=0; i--) {
		total += collection[i].second;
		if (elementToFind < total) return i;
	}
	return 0;
}

Histogram::Histogram() {
	for (int i=0; i<8; i++) {
		collection.push_back(std::make_pair(0, 0));
	}
}

void Histogram::AddReadStat(long time) {
	int bucketIdx = GetBucketNumber(time);
	collection[bucketIdx].first += 1; 
	read_size_++;
}

void Histogram::AddWriteStat(long time) {
	int bucketIdx = GetBucketNumber(time);
	collection[bucketIdx].second += 1;
	write_size_++;
}

double Histogram::GetPercentile(int type, double percentile) {
	int elementToFind;
	int bucket = -1;
	switch(type) {
		case 0: // read
			elementToFind = (percentile / 100.0) * read_size_;
			bucket = GetBucketFromReadElement(elementToFind);	
			break;
		case 1: // write
			elementToFind = (percentile / 100.0) * write_size_;
			bucket = GetBucketFromWriteElement(elementToFind);
			break;
		default:
			break;
	}
	return GetBucketAverage(bucket);
}
