#pragma once
#include <iostream>
#include <condition_variable>
#include <mutex>

class RWLock {
public:
	RWLock()
	: shared()
	, readerQ(), writerQ()
	, active_readers(0), waiting_writers(0), active_writers(0)
	{}

	void ReadLock();

	void ReadUnlock();

	void WriteLock();

	void WriteUnlock();

private:
	std::mutex shared;
	std::condition_variable readerQ;
	std::condition_variable writerQ;
	int active_readers;
	int waiting_writers;
	int active_writers;
};
