#include <iostream>
#include <condition_variable>
#include <mutex>

#include "RWLock.h"

void RWLock::ReadLock() {
	std::unique_lock<std::mutex> lk(shared);
	while (waiting_writers != 0) readerQ.wait(lk);
	++active_readers;
	lk.unlock();
}

void RWLock::ReadUnlock() {
	std::unique_lock<std::mutex> lk(shared);
	--active_readers;
	lk.unlock();
	writerQ.notify_one();	
}

void RWLock::WriteLock() {
	std::unique_lock<std::mutex> lk(shared);
	++waiting_writers;
	while (active_readers != 0 || active_writers != 0) writerQ.wait(lk);
	++active_writers;
	lk.unlock();
}

void RWLock::WriteUnlock() {
	std::unique_lock<std::mutex> lk(shared);
	--waiting_writers;
	--active_writers;
	if (waiting_writers > 0) 
		writerQ.notify_one();
	else 
		readerQ.notify_all();
	lk.unlock();
}
