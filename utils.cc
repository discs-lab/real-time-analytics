#include "utils.h"

template <typename T>
void PrintVector(T const &input) {
	std::cout << "[ ";
	for (auto it = input.begin(); it < input.end(); it++) {
		std::cout << *it << ", ";
	}
	std::cout << "]\n";
}

void PrintIntVector(std::vector<int> input) {
	for (auto const& elem : input) {
		std::cout << elem << ", ";
	}
	std::cout << std::endl;
}


void PrintStringVector(std::vector<std::string> input) {
	std::cout << "[ ";
	for (auto const& elem : input) {
		std::cout << elem << ", ";
	}
	std::cout << "]" << std::endl;
}


