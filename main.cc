#include <iostream>

#include "ImageAnnotationMap.h"
#include "ImageUserTimeSeriesMap.h"
#include "UserAnnotationTimeSeriesMap.h"
#include "UserAnnotationScoresMap.h"
#include "utils.h"

#include "benchmark.h"

int main() {
	RunBenchmark();
	/*			
	MapOne MAPONE;
	std::vector<int> v = {1, 2, 3, 4};
	MAPONE.Put(1, v);
	MAPONE.Put(5, v);
	PrintIntVector(MAPONE.Get(10));
	MAPONE.PrintContents();
	*/
	
	/*	
	MapTwo MAP_TWO;
	MAP_TWO.Put(1, 1, 1, "now");
	
	MAP_TWO.Put(1, 1, 1, "second");
	
	std::cout << MAP_TWO.Get(1, 1, 1);
	MAP_TWO.Put(1, 1, 1, "third");
	MAP_TWO.Put(1, 1, 2, "new1");
	MAP_TWO.Put(1, 2, 2, "new2");
	MAP_TWO.Put(2, 2, 2, "new3");
	MAP_TWO.PrintContents();
	*/

	/*
	MapThree MAP_THREE;
	MAP_THREE.Put(1, 1, 1, "now");
	MAP_THREE.Put(1, 1, 1, "second");
	MAP_THREE.Put(1, 1, 2, "second");
	MAP_THREE.Put(1, 3, 1, "second");
	MAP_THREE.Put(4, 1, 1, "second");
	MAP_THREE.PrintContents();
	*/

	/*	
	MapFour MAPFOUR;
	MAPFOUR.Put(1, 1, 1, 10); // normal put
	MAPFOUR.Put(0, 2, 2, 20); // actionID DNE
	MAPFOUR.PrintContents();
	*/	
}
