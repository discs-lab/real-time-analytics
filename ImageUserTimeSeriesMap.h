#pragma once
#include <iostream>
#include <vector>
#include <unordered_map>

#include "MapBase.h"

class MapTwo : public MapBase {
public:
	virtual void PrintContents() override;

	int Put(int /*userID*/, int /*imageID*/, int /*actionID*/, std::string /*timestamp*/);

	int Get(int /*userID*/, int /*imageID*/, int /*actionID*/);

private:
	std::unordered_map<int, std::unordered_map<int, std::unordered_map<int, std::pair<std::vector<std::string>, int>>>> store;

};
