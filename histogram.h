#pragma once
#include <iostream>
#include <vector>

// collection of buckets
// 0 - 1 us
// 1 - 5 us
// 5 - 10 us
// 10 - 50 us
// 50 - 100 us
// 100 - 1000 us
// 1 - 10 ms
// 10 - 100ms
// 
class Histogram {

public:
	Histogram();

	// adds stat to corresponding bucket based on time
	void AddReadStat(long);

	void AddWriteStat(long);

	// returns percentile for either read/write based on double value
	double GetPercentile(int /*0 for read, 1 for write*/, double);

	int GetBucketFromReadElement(int);

	int GetBucketFromWriteElement(int);

	void PrintReadCount();

	void PrintWriteCount();

private:
	// vector will have 8 buckets (each bucket is a value)
	// each element in vector contains tuple
	// first element in tuple is read value
	// second element in tuple is write value
	std::vector<std::pair<int, int>> collection;
	
	// number of elements in read element in all buckets
	int read_size_;

	// number of elements in write element in all buckets
	int write_size_;
};
